import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class FakeBackendService implements InMemoryDbService {
  createDb() {
    // type any, because we have added mapping
    const list: any[] = [
      {
        id: 1,
        title: 'Empire State Realty Trust, Inc.',
        type: 'video'
      },
      {
        id: 2,
        title: 'Morgan Stanley',
        type: 'document'
      },
      {
        id: 3,
        title: 'Nuveen Missouri Quality Municipal Income Fund',
        type: 'image'
      },
      {
        id: 4,
        title: 'Juniper Networks, Inc.',
        type: 'audio'
      },
      {
        id: 5,
        title: 'LMP Capital and Income Fund Inc.',
        type: 'video'
      },
      {
        id: 6,
        title: 'Bio-Rad Laboratories, Inc.',
        type: 'document'
      },
      {
        id: 7,
        title: 'Proteon Therapeutics, Inc.',
        type: 'image'
      },
      {
        id: 8,
        title: 'Sirius XM Holdings Inc.',
        type: 'audio'
      },
      {
        id: 9,
        title: 'Newtek Business Services Corp.',
        type: 'video'
      },
      {
        id: 10,
        title: 'PartnerRe Ltd.',
        type: 'document'
      },
      {
        id: 11,
        title: 'MFS Special Value Trust',
        type: 'image'
      },
      {
        id: 12,
        title: 'KBR, Inc.',
        type: 'audio'
      },
      {
        id: 13,
        title: 'Hartford Financial Services Group, Inc. (The)',
        type: 'video'
      },
      {
        id: 14,
        title: 'Mallinckrodt plc',
        type: 'document'
      },
      {
        id: 15,
        title: 'Lifeway Foods, Inc.',
        type: 'image'
      },
      {
        id: 16,
        title: 'WisdomTree U.S. Quality Dividend Growth Fund',
        type: 'audio'
      },
      {
        id: 17,
        title: 'Vanguard Russell 2000 Value ETF',
        type: 'video'
      },
      {
        id: 18,
        title: 'East West Bancorp, Inc.',
        type: 'document'
      },
      {
        id: 19,
        title: 'Azul S.A.',
        type: 'image'
      },
      {
        id: 20,
        title: 'StoneCastle Financial Corp',
        type: 'audio'
      }
    ];
    return {
      list
    };
  }
}
