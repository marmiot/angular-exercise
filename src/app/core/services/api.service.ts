import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ModelMapper} from '../helpers/model-mapper';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) {
  }

  public get<T>(url: string, itemType: any): Observable<T> {
    return this.http.get<T>(url).pipe(
      map((data: any) => {
        return data.map(item => new ModelMapper(itemType).map(item));
      })
    );
  }
}
