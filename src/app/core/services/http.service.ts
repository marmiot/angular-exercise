import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ListItem} from '../models/list-item.model';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  baseUrl = 'https://fakebackend.com/api/';
  listEndpoint = 'list';

  constructor(
    private api: ApiService
  ) {
  }

  getList(): Observable<ListItem[]> {
    return this.api.get<ListItem[]>(this.baseUrl + this.listEndpoint, ListItem);
  }
}
