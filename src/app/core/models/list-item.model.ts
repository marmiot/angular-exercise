import {propertyMap} from '../helpers/property-map';

export class ListItem {
  @propertyMap('id') public id: number;
  @propertyMap('title') public title: string;
  @propertyMap('type') public type: string;

  constructor() {
    this.id = null;
    this.title = null;
    this.type = null;
  }
}
