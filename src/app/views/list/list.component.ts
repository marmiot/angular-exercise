import {Component, OnDestroy, OnInit} from '@angular/core';
import {ListItem} from '../../core/models/list-item.model';
import {HttpService} from '../../core/services/http.service';
import {Observable, Subject, Subscription} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  list: ListItem[];
  filteredList$ = new Subject<ListItem[]>();

  filtersForm: FormGroup;
  filtersDefault = {
    searchQuery: '',
    type: '',
    order: '',
  };

  private filtersChanges: Subscription;

  currentPage: number;
  onPage = 10;

  constructor(
    private httpService: HttpService,
    private formBuilder: FormBuilder,
  ) {
  }

  ngOnInit(): void {
    this.httpService.getList().subscribe(
      (res: ListItem[]) => {
        this.list = res;
        this.filteredList$.next(res);
      },
      (error) => {
        console.log(error);
      }
    );

    this.filtersForm = this.formBuilder.group({
      searchQuery: [this.filtersDefault.searchQuery],
      type: [this.filtersDefault.type],
      order: [this.filtersDefault.order]
    });

    this.filtersChanges = this.filtersForm.valueChanges.subscribe(() => {
      this.provideFilters();
    });
  }

  ngOnDestroy(): void {
    this.filtersChanges.unsubscribe();
  }

  provideFilters(): void {
    this.filteredList$.next(
      this.filterSort(this.list, 'title', 'order')
        .filter((item: ListItem) => {
          return this.filterByQuery(item, 'title', 'searchQuery') && this.filterIsEqual(item, 'type', 'type');
        })
    );
  }

  filterByQuery(item: ListItem, key: string, filter: string): boolean {
    return item[key].toLocaleLowerCase().indexOf(this.filtersForm.controls[filter].value.toLocaleLowerCase()) > -1;
  }

  filterIsEqual(item: ListItem, key: string, filter: string): boolean {
    if (!this.filtersForm.controls[filter].value) {
      return true;
    }
    return (item[key] === this.filtersForm.controls[filter].value);
  }

  filterSort(array: ListItem[], key: string, filter: string): ListItem[] {
    const copyArray = array.slice();
    const collator = new Intl.Collator('en', {numeric: true, sensitivity: 'variant'});
    let returnedArray: ListItem[];

    switch (this.filtersForm.controls[filter].value) {
      case 'ASC':
        returnedArray = copyArray.sort((a, b) => collator.compare(a[key], b[key]));
        break;
      case 'DESC':
        returnedArray = copyArray.sort((a, b) => collator.compare(b[key], a[key]));
        break;
      default:
        returnedArray = array;
    }

    return returnedArray;
  }

  clearFilters(): void {
    this.filtersForm.patchValue(this.filtersDefault);
  }

}
