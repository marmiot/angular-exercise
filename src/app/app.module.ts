import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ListComponent} from './views/list/list.component';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {FakeBackendService} from './core/services/fake-backend.service';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
    InMemoryWebApiModule.forRoot(FakeBackendService) // Fake backend
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
